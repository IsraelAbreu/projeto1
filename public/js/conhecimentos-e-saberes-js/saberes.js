let globalSaberId = 0;

//SALVAR SABER
const salvarSaber = () => {
  const conhecimento = $('#select-conhecimento').val()
  const descricao = $('#descricao-saber').val()

  if(descricao != ""){
    $.ajax({
        url: "/saberes/create",
        type: "post",
        data: {
            conhecimento,
            descricao,            
            _token
        },
        success: function(data){

          const response = typeof data == 'string' ? JSON.parse(data) : data

          alert('Mensagem: '+ response.msg)

          if(!response.error){            
            $("#tbody-saber").append(`
            <tr class="tr-campo-saber-${response.saber.id}">
              <td scope="row">${response.saber.id}</td>
              <td>${response.saber.descricao}</td>
              <td>${response.saber.conhecimento.descricao}</td>
              <td>
              <button type="button" class="btn btn-primary" id="btnEditarSaberes" onclick="abriModalParaEditarSaberes(${response.saber.id})">
              Editar
              </button>
              </td>
            </tr>
            `);
            }
        }
    });
  }else{
    alert('Preencha todos os campos');
  }       
}
//ABRIR MODAL SABER -  GET
const abriModalParaEditarSaberes = (saberId) => {
  $.ajax({
    type: "get",
    url: `/edit/saberes/${saberId}`,
    success: function (response){
        $("#descricao-saber-update").val(response.descricao);
        $("#input-id-edit-saber").val(response.id);
        $("#select-conhecimento-saber").val(response.saberes_conhecimento_id);
        $("#modal-edit-saberes").modal("show");
        globalSaberId = saberId;
    }    
  })
}

const SalvarUpdateSaber = () =>{
  const conhecimento = $('#select-conhecimento').val();
  const descricao = $('#descricao-saber-update').val();
  if(descricao != ""){
    $.ajax({
        type: "put",
        url: `/edit/saberes/${globalSaberId}`,       
        data: {
            conhecimento,
            descricao,            
            _token
        },
        success: function(data){
          const response = typeof data == 'string' ? JSON.parse(data) : data

          alert('Mensagem: '+ response.msg)

          console.log(data);

          if(!response.error){            
            $(`.tr-campo-saber-${response.saber.id}`).html(`            
                <td scope="row">${response.saber.id}</td>
                <td>${response.saber.descricao}</td>
                <td>${response.saber.conhecimento.descricao}</td>
                <td>
                  <button type="button" class="btn btn-primary" id="btnEditarSaberes" onclick="abriModalParaEditarSaberes(${response.saber.id})">
                  Editar
                  </button>
                </td>          
            `);
            }
        }
    })
  } else {
    alert('Preencha todos os campos')
  }
}

//DELETE SABER
const deletarSaber = () => {
  $.ajax({
    type: "delete",
    url: `/delete/saberes/${globalSaberId}`,
    data: {
      _token
    },
    success: function (data) {
      alert('Mensagem: '+ data.msg)

      const response = typeof data == 'string' ? JSON.parse(data) : data

      if(!response.error){
        $(`.tr-campo-saber-${globalSaberId}`).remove();
      }
      console.log("saber deletado com sucesso");
      $("#modal-edit-saberes").modal("hide");
    }
  });
}

$(document).ready(function () {
  $("#btn-salvar-saber").click(function(){
  salvarSaber();
  });

  $('#btn-modal-campo-saber').click(function(){
    $('#ModalSaber').modal('show');
  });  
});
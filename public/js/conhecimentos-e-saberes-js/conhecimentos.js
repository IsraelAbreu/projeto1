const _token = $('#_token').val()
let globalConhecimentoId = 0;
//Store
const salvarConhecimento = () => {
  const descricao = $('#descricao-conhecimento').val()

  if(descricao != ""){
    $.ajax({
        url: "/create/conhecimento",
        type: "POST",
        data: {
            descricao,            
            _token
        },
        success: function(data){

          const response = typeof data == 'string' ? JSON.parse(data) : data

          alert('Mensagem: '+ response.msg)

          if(!response.error){
            $(`#table-conhecimento tbody`).append(`           
              <tr class="tr-campo-conhecimento-${response.conhecimento.id}">
                <th>${response.conhecimento.id}</th>
                <td>${response.conhecimento.descricao}</td>
                <td>
                  <button type="button" class="btn btn-primary" onclick="abrirModalParaEditarConhecimento(${response.conhecimento.id})">
                      Editar
                  </button>
                </td>
              </tr>
            `);
          }       
        }
    });
  }else{
    alert('Preencha todos os campos')
  }       
}


//Update Conhecimento:
const abrirModalParaEditarConhecimento = (conhecimentoId) => {

  $.ajax({
      type: "get",
      url: `/edit/conhecimento/${conhecimentoId}`,
      success: function (response){
          $("#descricao-conhecimento-update").val(response.descricao);
          $("#input-id-edit-conhecimento").val(response.id);
          $("#modal-edit-conhecimentos").modal("show")
          globalConhecimentoId = conhecimentoId;
      }
  });       
}
const SalvarUpdateConhecimento = () =>{  
  const descricao = $("#descricao-conhecimento-update").val()
  
  if(descricao != ""){
    $.ajax({
      type: "put",
      url: `/edit/conhecimento/${globalConhecimentoId}`,
      data:{
        descricao,
        _token
      },
      success: function (data) {
        const response = typeof data == 'string' ? JSON.parse(data) : data
        
        alert('Mensagem: '+ response.msg)
        
        if(!response.error){ 
          $(`.tr-campo-conhecimento-${response.conhecimento.id}`).html(`           
              <td>${response.conhecimento.id}</td>
              <td>${response.conhecimento.descricao}</td>
              <td>
                <button type="button" class="btn btn-primary" onclick="abrirModalParaEditarConhecimento(${response.conhecimento.id})">
                    Editar
                </button>
              </td>
          `);
        }
      }
    });
  } else{
    alert('Preencha todos os campos')
  }
}

const deletarConhecimento = () =>{       
  $.ajax({
      type: "delete",
      url:`/delete/conhecimento/${globalConhecimentoId}`,
      data: {
          _token
      },
      success: function (data) {

        alert('Mensagem: '+ data.msg)

        const response = typeof data == 'string' ? JSON.parse(data) : data
        
        if(!response.error){
          //location.reload();
          $(`.tr-campo-conhecimento-${globalConhecimentoId}`).remove();
        }

        $("#modal-edit-conhecimentos").modal("hide");
        
      }
  });
}

$(document).ready(function () {
  $("#btn-salvar-conhecimento").click(function(){
    salvarConhecimento()
    
    location.reload(true)
  })
  $("#btn-update-conhecimento").click(function (e) {
    SalvarUpdateConhecimento() 
    location.reload(true)    
  });
  $('#btn-modal-campo-conhecimento').click(function(){
    $("#ModalConhecimentos").modal('show')
  })
})

let docenteIdGlobal = 0;
const _token = $('#_token').val();

const salvarDocente = () => {
  const nome = $('#nomeDocente').val();
  const matricula = $('#matriculaDocente').val();  

  const form = {
    _token,
    nome,
    matricula
  }
  if (nome && matricula != "") {
    $.ajax({
      type: "POST",
      url: "/create/docente",
      data: form,
      success: function (data) {
        const response = typeof data == 'string' ? JSON.parse(data) : data
        
        alert('Mensagem: '+ response.msg)
        if(!response.error){`
          <tr>
            <td scope="row">${response.docente.id}</td>
            <td>${response.docente.nome}</td>
            <td>${response.docente.matricula}</td>
            <td>
              <button id="BtnEditarDocente" type="button" class="btn btn-primary" onclick="EditarDocente(${response.docente.id})">
                Editar
              </button>
            </td>
          </tr>` 
        }
      },
      error: function (xhr, msg, status) { 
        console.log(msg, status);
       }
    });
  } else {
    alert('Preencha todos os campos')
  }
}

//ABRIR UPDATE
const EditarDocente = (docenteId) => {
  $.ajax({
    type: "GET",
    url: `/edit/docente/${docenteId}`,
    success: function (response) {
      $("#nomeDocenteUpdate").val(response.nome);
      $("#matriculaDocenteUpdate").val(response.matricula);
      $("#ModalEditarDocente").modal("show")
      docenteIdGlobal = docenteId;
    }
  });
}

//SUBMIT UPDATE
const salvarDocenteUpdate = (docenteId) => {
  const nome = $('#nomeDocenteUpdate').val();
  const matricula = $('#matriculaDocenteUpdate').val();

  const form = {
    _token,
    nome,
    matricula
  }
  if (nome && matricula != "") {
    $.ajax({
      type: "PUT",
      url: `/edit/docente/${docenteIdGlobal}`,
      data: form,
      success: function (data) {
        const response = typeof data == 'string' ? JSON.parse(data) : data
        
        alert('Mensagem: '+ response.msg)
        if(!response.error){
          $(`.tr trDocente${response.docente.id}`).html(`
            <td scope="row">${response.docente.id}</td>
            <td>${response.docente.nome}</td>
            <td>${response.docente.matricula}</td>
            <td>
              <button id="BtnEditarDocente" type="button" class="btn btn-primary" onclick="EditarDocente(${response.docente.id})">
                Editar
              </button>
            </td>
        </tr>`);
        }
      },
    });
  } else {
    alert('Preencha todos os campos')
  }
}

//DELETE DOCENTE
const deletarDocente = () => {
  $.ajax({
    type: "delete",
    url: `/delete/docente/${docenteIdGlobal}`,
    data: {
      _token
    },
    success: function () {
      alert("Docente deletado")
    }
  });
}



$(document).ready(function () {
  $("#btnModalCadastroDocentes").click(function(){ 
    $("#ModalDocente").modal('show');
  });
  $('#btnSalvarDocente').click(function(){ 
    salvarDocente()
    location.reload(true);
  });
  
  $("#btnSalvarDocenteUpdate").click(function() { 
    //location.reload(true);
  });
  
  $("#btnDeletarDocente").click(function () { 
    //location.reload(true);
  });
});


@extends('layout.layout-docente')
<div class="modal fade" id="ModalEditarDocente" tabindex="-1" aria-labelledby="" aria-hidden="true" data-bs-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="">Editar docente</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form>
          <input id="_token" type="hidden" name="">
          <div class="mb-3">
            <label for="nomeDocente" class="form-label">Nome completo</label>
            <input type="text" class="form-control" id="nomeDocente">
          </div>
          <div class="mb-3">
            <label for="matriculaDocente" class="form-label">Matricula</label>
            <input type="text" class="form-control" id="matriculaDocente">
          </div>         
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-success" id="btnSalvarEdicaoDocente">Salvar</button>
      </div>
    </div>
  </div>
</div>
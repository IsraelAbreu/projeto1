@extends('docentes-e-avaliacao.layout.layout-docentes')
@section('conteudo')
<div class="row">
  <div class="col mx-3 mt-5">
    <div class="card border-primary">
      <div class="card-header text-bg-secondary">
        <div id="">
          <h5>Docentes</h5>
        </div>           
      </div>
      <div class="card-body">
        <table class="table">
          <thead>
            <tr>
              <th scope="col">ID</th>
              <th scope="col">Nome</th>
              <th scope="col">Matricula</th>
              <th scope="col">Ação</th>
            </tr>
          </thead>
          <tbody id="tbody-saber">         
            @foreach ($dadosDocente as $docente)
              <tr class="trDocente{{$docente->id}}">
                <td scope="row">{{$docente->id}}</td>
                <td>{{$docente->nome}}</td>
                <td>{{$docente->matricula}}</td>
                <td>
                  <button id="BtnEditarDocente" type="button" class="btn btn-warning"  onclick=EditarDocente({{$docente->id}})>
                    Editar
                  </button>
                </td>
              </tr>  
            @endforeach          
          </tbody>
        </table>
      </div> 
    </div>
  </div>
<!--Fim Table docente-->

<!--Table avaliacao-->
  <div class="col mx-3 mt-5">
    <div class="card border-primary">
      <div class="card-header text-bg-secondary">
        <div id="">
          <h5>Avaliações</h5>
        </div>           
      </div>
      <div class="card-body">
        <table class="table">
          <thead>
            <tr>
              <th scope="col">ID</th>
              <th scope="col">Nome</th>
              <th scope="col">saber</th>
              {{-- <th scope="col">Conhecimento</th> --}}
              <th scope="col">Ações</th>
            </tr>
          </thead>
          <tbody id="tbody-saber">         
            @foreach ($dadosAvaliacao as $avaliacao)
              <tr>
                <td>1</td>
                <td>{{$avaliacao->nome}}</td>
                <td>{{$avaliacao->getSaber->descricao}}</td>
                <td>
                  <button type="button" class="btn btn-warning">
                    Editar
                  </button>
                </td>`
              </tr> 
            @endforeach           
          </tbody>
        </table>
      </div> 
    </div>
  </div>
</div>

<input type="hidden" id="_token" value="{{csrf_token()}}">
<!--MODAL PARA CADASTRO DE DOCENTES-->
<div class="modal fade" id="ModalDocente" tabindex="-1" aria-labelledby="" aria-hidden="true" data-bs-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="">Registro de docentes</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form>
          <input id="_token" type="hidden" name="">
          <div class="mb-3">
            <label for="nomeDocente" class="form-label">Nome completo</label>
            <input type="text" class="form-control" id="nomeDocente">
          </div>
          <div class="mb-3">
            <label for="matriculaDocente" class="form-label">Matricula</label>
            <input type="text" class="form-control" id="matriculaDocente">
          </div>         
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-success" id="btnSalvarDocente">Salvar</button>
      </div>
    </div>
  </div>
</div>

<!--MODAL PARA CADASTRO DE AVALIACAO-->
<div class="modal fade" id="ModalCriarAvaliacao" tabindex="-1" aria-labelledby="" aria-hidden="true" data-bs-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="">Registro de Avaliação</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form>
          <input id="_token" type="hidden" name="">
          <div class="mb-3">
            <label for="nomeDocente" class="form-label">Nome</label>
            <input type="text" class="form-control" id="input-nome">
          </div>
          <div class="mb-3">
            <label for="matriculaDocente" class="form-label">Saber</label>
              <select class="form-control" id="select-saber">
                @foreach ($dadosSaberes as $saber)
                  <option value="{{ $saber->id}}">{{$saber->descricao}}</option>
                @endforeach
              </select>
          </div>
          {{-- <div class="mb-3">
            <label for="matriculaDocente" class="form-label">Conhecimento</label>
              <select class="form-control" id="select-conhecimento">
                @foreach ($dadosConhecimento as $conhecimento)
                    <option value="{{ $conhecimento->id}}">{{$conhecimento->descricao}}</option>
                @endforeach
              </select>
          </div>         --}}
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-success" id="btnSalvarAvaliacao">Salvar</button>
      </div>
    </div>
  </div>
</div>


<!--MODAL EDIT DOCENTES-->
<div class="modal fade" id="ModalEditarDocente" tabindex="-1" aria-labelledby="" aria-hidden="true" data-bs-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="">Editar docente</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form>
          <input id="_token" type="hidden" name="">
          <div class="mb-3">
            <label for="nomeDocente" class="form-label">Nome completo</label>
            <input type="text" class="form-control" id="nomeDocenteUpdate">
          </div>
          <div class="mb-3">
            <label for="matriculaDocente" class="form-label">Matricula</label>
            <input type="text" class="form-control" id="matriculaDocenteUpdate">
          </div>         
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" id="btnDeletarDocente" onclick="deletarDocente()">Deletar</button>
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-success" id="btnSalvarDocenteUpdate" onclick="salvarDocenteUpdate()">Salvar</button>
      </div>
    </div>
  </div>
</div>
@endsection
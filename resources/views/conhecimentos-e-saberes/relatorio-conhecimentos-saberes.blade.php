<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Relatório</title>
  <style>
    table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
    }

    td, th {
    border: 1px solid #dddddd;
    text-align: center;
    padding: 8px;
    }
  </style>
</head>
<body>
  <div class="container">
    <h1>
      Relatório completo
    </h1>
        <!--Table Conhecimentos-->
          <div >
            <div >
              <div >
                <h3>Conhecimentos cadastrados</h3>
              </div>
            </div>
            <div>              
              <table class="table">
                <thead>
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Descricao</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($conhecimentos as $conhecimento)
                  <tr class="tr-campo-conhecimento-{{$conhecimento->id}}">
                    <th scope="row">{{$conhecimento->id}}</th>
                    <td>{{$conhecimento->descricao}}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <!--Table Saberes-->
            <div>
              <div>
                <h3>Saberes cadastrados</h3>
              </div>           
            </div>
            <div class="card-body">
              <table class="table">
                <thead>
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Saber</th>
                    <th scope="col">Áre de conhecimento</th>
                  </tr>
                </thead>
                <tbody id="">
                  @foreach ($saberes as $saber)
                    <tr class="tr-campo-saber-{{$saber->id}}">
                      <td scope="row">{{$saber->id}}</td>
                      <td>{{$saber->descricao}}</td>
                      <td>{{$saber->conhecimento->descricao}}</td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div> 
</body>
</html>

<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <!--Jquery CDN-->
  <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
  <!-- CSS only -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
  <title>LNT</title>
  <style>
   .btn-primary, .btn-success, .nav-item{
      width: 270px;
    }
  </style>
</head>
<body>
  <div class="container mt-1">
    <nav class="navbar navbar-expand-lg bg-light">
      <div class="container-fluid">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item mx-2">
              <a class="btn btn-primary nav-item" href="{{route('index.route')}}">Conhecimentos | Saberes</a>
            </li> 
            <li class="nav-item mx-2">
              <a class="btn btn-primary nav-item" id="docentes" href="{{route('docente-index')}}">Docentes | Avaliações</a>
            </li>         
          </ul>
          <form class="d-flex" role="search">
              <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
              <button class="btn btn-outline-success" type="submit" id="btn-testar">Procurar</button>
          </form>
        </div>
      </div>
    </nav>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-9">
        @yield('ModalPrincipal')
        </div>
      </div>
      
      <div class="col-3 mt-5">
        <div class="d-flex flex-column flex-shrink-0 p-3 bg-light">
          <a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto link-dark text-decoration-none">
            <svg class="bi pe-none me-2" width="40" height="32"><use xlink:href="#bootstrap"/></svg>
            <span class="fs-4">Menu de opções</span>
          </a>
          <hr>
          <ul class="nav nav-pills flex-column mb-auto">
            <li class="nav-item">
              <a class="btn btn-primary nav-item" id="btn-modal-campo-conhecimento">
                <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#home"/></svg>
                  Cadastrar conhecimento
              </a>
            </li>
            <li class="nav-item mt-2">
              <a class="btn btn-primary nav-item" href="#" data-bs-toggle="modal" data-bs-target="#ModalSaber" id="">
                Cadastrar saber
                <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#home"/></svg>
              </a>
            </li>
            <li class="nav-item mt-2">
              <a class="btn btn-success nav-item" href="{{route('pdf')}}">Gerar relatório</a>
            </li>
          </ul>
        </div> 
      </div>
    </div>
  </div>
  
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
  <!-- JavaScript Bundle with Popper -->
  <script type="text/javascript" src="{{ asset('js/conhecimentos-e-saberes-js/conhecimentos.js')}}"></script>
  <script type="text/javascript" src="{{ asset('js/conhecimentos-e-saberes-js/saberes.js')}}"></script>
</body>
</html>
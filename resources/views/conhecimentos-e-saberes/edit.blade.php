@extends('conhecimentos-e-saberes.layout.app')
@section('ModalPrincipal')
<div class="modal fade" id="modal-edit-conhecimentos" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" data-bs-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="titulo_Modal_Conhecimento">Atualizar campo de conhecimento</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form>
          <input id="_token" type="hidden" name="" value="{{csrf_token()}}">
          {{-- <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">ID</label>
            <input type="text" class="form-control" id="id_conhecimento">
          </div> --}}
          <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Descrição</label>
            <input type="text" class="form-control" id="descricao-conhecimento">
          </div>        
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-success" id="btn-salvar-conhecimento">Salvar</button>
        <button type="button" class="btn btn-danger">
          Excluir
        </button>
      </div>
    </div>
  </div>
</div>
@endsection
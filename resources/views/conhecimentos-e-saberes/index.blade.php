@extends('conhecimentos-e-saberes.layout.app')
@section('ModalPrincipal')  
    <!-- Modal Conhecimentos-->
    <div class="modal fade" id="ModalConhecimentos" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" data-bs-backdrop="static">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="titulo_Modal_Conhecimento">Criar campo de conhecimento</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <form>
              <input id="_token" type="hidden" name="" value="{{csrf_token()}}">
              <div class="mb-3">
                <label for="descricao-conhecimento" class="form-label">Descrição</label>
                <input type="text" class="form-control" id="descricao-conhecimento">
              </div>        
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
            <button type="button" class="btn btn-success" id="btn-salvar-conhecimento">Salvar</button>
          </div>
        </div>
      </div>
    </div>    
    <!-- Modal Saberes-->
    <div class="modal fade" id="ModalSaber" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="titulo_modal_Saber">Cadastrar campo de saber</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <form>
              <div class="">
                <label for="">Conhecimento</label>
                <select class="form-control" id="select-conhecimento">
                  @foreach ($dadosConhecimento as $conhecimento)
                    <option value="{{ $conhecimento->id}}">{{$conhecimento->descricao}}</option>
                  @endforeach
                </select>
              </div>
              <br>
              <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Descrição</label>
                <input type="text" class="form-control" id="descricao-saber">
              </div>        
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
            <button type="button" class="btn btn-success" id="btn-salvar-saber">Salvar</button>
          </div>
        </div>
      </div>
    </div>
    <!--Tables-->
    <div class="container text-center mt-5">
      <div class="row">
        <!--Table Conhecimentos-->
        <div class="col mx-3">
          <div class="card card border-primary">
            <div class="card-header text-bg-secondary">
              <div id="titulo_Tabela_Saberes">
                <h5>Conhecimentos</h5>
              </div>
            </div>
            <div class="card-body">              
              <table class="table" id="table-conhecimento">
                <thead>
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Descricao</th>
                    <th scope="col">Ações</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($dadosConhecimento as $conhecimento)
                  <tr class="tr-campo-conhecimento-{{$conhecimento->id}}">
                    <th scope="row">{{$conhecimento->id}}</th>
                    <td>{{$conhecimento->descricao}}</td>
                    <td>
                      <button type="button" class="btn btn-warning" onclick="abrirModalParaEditarConhecimento({{$conhecimento->id}})">
                        Editar
                      </button>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <!--Table Saberes-->
        <div class="col mx-3">
          <div class="card border-primary">
            <div class="card-header text-bg-secondary">
              <div id="titulo_Tabela_Saberes">
                <h5>Saberes</h5>
              </div>           
            </div>
            <div class="card-body">
              <table class="table">
                <thead>
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Saber</th>
                    <th scope="col">Áre de conhecimento</th>
                    <th scope="col">Ações</th>
                  </tr>
                </thead>
                <tbody id="tbody-saber">
                  @foreach ($dadosSaberes as $saber)
                    <tr class="tr-campo-saber-{{$saber->id}}">
                      <td scope="row">{{$saber->id}}</td>
                      <td>{{$saber->descricao}}</td>
                      <td>{{$saber->conhecimento->descricao}}</td>
                      <td>
                        <button type="button" class="btn btn-warning" id="btnEditarSaberes" onclick="abriModalParaEditarSaberes({{$saber->id}})">
                          Editar
                        </button>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div> 
          </div>
        </div>
    </div>
<!--MODAL DE UPDATE Conhecimento-->
<div class="modal fade" id="modal-edit-conhecimentos" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" data-bs-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="titulo_Modal_Conhecimento">Atualizar campo de conhecimento</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form>
          <div class="mb-3">
            <input type="hidden" class="form-control" id="input-id-edit-conhecimento">
          </div>
          <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Descrição</label>
            <input type="text" class="form-control" id="descricao-conhecimento-update">
          </div>        
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-success" id="btn-update-conhecimento" onclick="SalvarUpdateConhecimento()">Salvar</button>
        <button type="button" class="btn btn-danger" id="btn-excluir-conhecimento" onclick="deletarConhecimento()">
          Excluir
        </button>
      </div>
    </div>
  </div>
</div>

<!--MODAL DE UPDATE Saberes-->
<div class="modal fade" id="modal-edit-saberes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" data-bs-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="titulo_Modal_Conhecimento">Atualizar campo de saber</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form>
          <div class="mb-3">
            <input type="hidden" class="form-control" id="input-id-edit-saber">
          </div>
          <div class="">
            <label for="select-conhecimento-saber">Conhecimento</label>
            <select class="form-control" id="select-conhecimento-saber">
              @foreach ($dadosConhecimento as $conhecimento)
                <option value="{{ $conhecimento->id }}">{{$conhecimento->descricao}}</option>
              @endforeach
            </select>
          </div>
          <div class="mb-3">
            <label for="" class="form-label">Descrição</label>
            <input type="text" class="form-control" id="descricao-saber-update">
          </div>        
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-success" id="btn-update-saber" onclick="SalvarUpdateSaber()">Salvar</button>
        <button type="button" class="btn btn-danger" id="btn-excluir-saber" onclick="deletarSaber()">
          Excluir
        </button>
      </div>
    </div>
  </div>
</div>
@endsection
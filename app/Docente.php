<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Docente extends Model
{
    //
    protected $fillable = ['id', 'nome', 'matricula'];
    public function avaliacoes(){
        return $this->belongsToMany(Avalicao::class);
    }
}

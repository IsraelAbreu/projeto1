<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Conhecimentos;

class Saberes extends Model
{
    //
    protected $table = 'saberes';
    protected $fillable = ['id','descricao', 'saberes_conhecimento_id'];
    Public function conhecimento(){
        return $this->belongsTo(Conhecimentos::class, 'saberes_conhecimento_id', 'id');
    }
}

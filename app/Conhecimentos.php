<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Saberes;

class Conhecimentos extends Model
{
    //
    protected $table = 'conhecimentos';
    protected $fillable = ['descricao'];
    
    public function saberes(){
        return $this->hasMany(Saberes::class);
    }
}

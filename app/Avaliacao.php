<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Avaliacao extends Model
{
    protected $table = 'avaliacoes';
    protected $fillable = [
        'nome',
        'conhecimento',
        'saber',
    ];
    public function docentes(){
        return $this->belongsToMany(Docente::class);
    }

    public function getSaber(){
        return $this->belongsTo(Saberes::class, 'saber', 'id');
    }
}

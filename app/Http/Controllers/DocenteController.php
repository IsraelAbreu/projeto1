<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Docente;
use App\Saberes;
use App\Conhecimentos;
use App\Avaliacao;

class DocenteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dadosDocente = Docente::all();
        $dadosSaberes = Saberes::with('conhecimento')->get();
        $dadosConhecimento = Conhecimentos::all();
        $dadosAvaliacao = Avaliacao::with('getSaber')->get();

        return view('docentes-e-avaliacao.index', [
            'dadosDocente'      => $dadosDocente,
            'dadosSaberes'      => $dadosSaberes,
            'dadosConhecimento' => $dadosConhecimento,
            'dadosAvaliacao'    => $dadosAvaliacao
        ]);
    }

    public function store(Request $request)
    {
        try {
            $dadosDocente = Docente::create($request->all());

            if (!$dadosDocente) {
                throw new Exception("Erro ao cadastrar docente");
            }
            $response['error']          = false;
            $response['msg']            = 'Docente cadastrado';
            $response['dadosDocente']   = $dadosDocente;
        } catch (\Throwable $ex) {
            $response['error']         = true;
            $response['msg']           = $ex->getMessage();
        } finally {
            return response()->json($response);
        }
    }

    public function edit($id)
    {
        $docente = Docente::find($id);
        return response($docente, 200);
    }

    public function update(Request $request, $id)
    {
        $docente = Docente::find($id);

        try {
            if (!$docente->update($request->all())) {
                throw new \Exception('Erro ao atualizar dados do docente');
            }
            $response['error']          = false;
            $response['msg']            = 'dados atualizados';
            $response['conhecimento']   = $docente;
        } catch (\Exception $ex) {
            $response['error'] = true;
            $response['msg']   = $ex->getMessage();
        } finally {
            return response()->json($response);
        }
    }

    public function destroy($id)
    {
        $docente = Docente::find($id);
        $docente->delete();
    }
}

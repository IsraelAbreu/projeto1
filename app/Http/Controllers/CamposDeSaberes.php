<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Saberes;

class CamposDeSaberes extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $dadosSaberes = Saberes::all();
        // return view('index', compact('dadosSaberes'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        try {
            $data = [
                'saberes_conhecimento_id' => $request->conhecimento,
                'descricao' => $request->descricao
            ];
            $saber = Saberes::create($data);
            
            if(!$saber){
                throw new \Exception("Não foi possivel cadastrar o saber");
            }
            $response['error']          = false;
            $response['msg']            = 'deu certo';
            $response['saber']   = $saber;

            //Lazer Eager Load
            $saber->load('conhecimento');

            return response()->json([
                'saber'=>$saber
            ]);
        } catch (\Exception $ex) {
           $response['error'] = true;
           $response['msg']   = $ex->getMessage();
        } finally {
            return response()->json($response);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $saber = Saberes::find($id);
        return response($saber, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $saber = Saberes::with('conhecimento')->find($id);

        try {
            if(!$saber->update($request->all())){
                throw new Exception("Errro ao atualizar saber");                
            }
            $response['error']  = false;
            $response['msg']    = 'saber atualizado';
            $response['saber']   = $saber;
        } catch (\Exception $ex) {
            $response['error']  = true;
            $response['msg']    = $ex->getMessage();
        } finally {
            return response()->json($response); 
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $saberes = Saberes::find($id);

        try {
            if(!$saberes->delete()){
                throw new Exception("Erro ao deletar saber");
                
            }
            $response['error']  =  false;
            $response['msg']    = 'Saber deletado';
        } catch (\Exception $ex) {

            $response['error']  = true;
            $response['msg']    = $ex->getMessaget();
        } finally {
            return response()->json($response);
        }
    }
}

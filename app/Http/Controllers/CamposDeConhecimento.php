<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Conhecimentos;
use App\Saberes;
use DB;

class CamposDeConhecimento extends Controller
{

    public function index()
    {
        //
        // return dd($dadosSaberes);
        $dadosSaberes = Saberes::with('conhecimento')->get();
        $dadosConhecimento = Conhecimentos::all();
        return view('conhecimentos-e-saberes.index', compact('dadosConhecimento', 'dadosSaberes'));

    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        
        try {
            $conhecimento = Conhecimentos::create($request->all());
            
            if(!$conhecimento) {
                throw new \Exception('Erro ao cadastrar conhecimento');
            }

            $response['error']          = false;
            $response['msg']            = 'Conhecimento cadastrado';
            $response['conhecimento']   = $conhecimento;
        } catch (\Exception $ex) {
            $response['error'] = true;
            $response['msg']   = $ex->getMessage();
        } finally {
            return response()->json($response);
        }
    }


    public function show($id)
    {

    }


    public function edit($id)
    {
        //
        $conhecimento = Conhecimentos::find($id);
        return response($conhecimento, 200);
    }


    public function update(Request $request, $id)
    {
        //
        $conhecimento = Conhecimentos::find($id);

        try {
            if (!$conhecimento->update($request->all())){
                throw new \Exception('Erro ao atualizar conhecimento');
            }
            $response['error']          = false;
            $response['msg']            = 'Conhecimento atualizado';
            $response['conhecimento']   = $conhecimento;
        } catch (\Exception $ex) {
            $response['error'] = true;
            $response['msg']   = $ex->getMessage();
        } finally {
            return response()->json($response);
        }
       
    }


    public function destroy($id)
    {
        //
        $conhecimento = Conhecimentos::find($id);

       try {
            if (!$conhecimento->delete()) {
                throw new \Exception('Erro ao deletar conhecimento');
            }
            $response['error']   = false;
            $response['msg']     = 'Conhecimento deletado';
       } catch (\Exception $ex) {
            $response['error'] = true;
            $response['msg']   = $ex->getMessage();
       } finally {
            return response()->json($response);
       }
    }
}

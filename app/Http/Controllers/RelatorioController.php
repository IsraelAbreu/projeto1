<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Conhecimentos;
use App\Saberes;
use PDF;

class RelatorioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $dadosSaberes = Saberes::with('conhecimento')->get();
        $dadosConhecimento = Conhecimentos::all();
        return view('conhecimentos-e-saberes.relatorio-conhecimentos-saberes', compact('dadosConhecimento', 'dadosSaberes'));
    }

    public function gerarRelatorio()
    {
        $conhecimentos = Conhecimentos::all();
        $saberes = Saberes::with('conhecimento')->get();
        $pdf = PDF::loadView('conhecimentos-e-saberes.relatorio-conhecimentos-saberes', compact('conhecimentos', 'saberes'));
        return $pdf->stream('conhecimentos&saberes.pdf');
    }
}

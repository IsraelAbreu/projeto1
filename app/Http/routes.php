<?php
use App\Http\Controllers\CamposDeConhecimento;
use App\Http\Controllers\CamposDeSaberes;
use App\Http\Controllers\RelatorioController;
use App\Http\Controllers\DocenteController;
use App\Http\Controllers\AvaliacaoController;

//ROTAS PARA O CAMPO DE CONHECIMENTO
Route::get('/','CamposDeConhecimento@index')->name('index.route');
Route::post('/create/conhecimento','CamposDeConhecimento@store');
Route::get('/edit/conhecimento/{id}','CamposDeConhecimento@edit');
Route::put('/edit/conhecimento/{id}','CamposDeConhecimento@update');
Route::delete('/delete/conhecimento/{id}','CamposDeConhecimento@destroy');
//ROTAS PARA SABARES
Route::post('/saberes/create', 'CamposDeSaberes@store');
Route::get('/edit/saberes/{id}','CamposDeSaberes@edit');
Route::put('/edit/saberes/{id}','CamposDeSaberes@update');
Route::delete('/delete/saberes/{id}','CamposDeSaberes@destroy');

//relatorio
Route::get('/pdf', 'RelatorioController@gerarRelatorio')->name('pdf');

//Docentes
Route::get('/docentes', 'DocenteController@index')->name('docente-index');
Route::post('/create/docente', 'DocenteController@store');
Route::get('/edit/docente/{id}', 'DocenteController@edit');
Route::put('/edit/docente/{id}', 'DocenteController@update');
Route::delete('/delete/docente/{id}', 'DocenteController@destroy');

//Avaliacao
Route::post('/store/avaliacao', 'AvaliacaoController@store');
Route::get('/edit/avaliacao/{id}', 'AvaliacaoController@edit');
Route::put('/edit/avaliacao/{id}', 'AvaliacaoController@update');
Route::delete('/delete/avaliacao/{id}', 'AvaliacaoController@destroy');
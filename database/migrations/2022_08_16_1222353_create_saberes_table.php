<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaberesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('saberes', function (Blueprint $table) {
            $table->increments('id');
            $table->text('descricao');
            $table->timestamps();
            $table->integer('saberes_conhecimento_id')->unsigned();
            $table->foreign('saberes_conhecimento_id')->references('id')->on('conhecimentos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('saberes', function (Blueprint $table){
            $table->foreign('saberes_conhecimento_id')->references('id')->on('conhecimentos')->onDelete('cascade');
        });
       
    }
}
